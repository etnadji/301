# I left Github. Here is where my repos were moved.

## EN

In response to Microsoft's acquisition of Github, my repositories are now 
available on other platforms. My GitHub repositories (except this one, 
obviously) have been deleted. Because of the importance of GitHub, I can not 
afford to be missing on it. But I can limit the damages by linking to other 
platforms, until I host my repositories myself.

(Yes, this is bad english.)

> [Why 301?](https://en.wikipedia.org/wiki/HTTP_301)

> [Also, Microsoft products stinks](https://itvision.altervista.org/why-windows-10-sucks.html)

## FR

En réponse à l’acquisition de Github par Microsoft, mes dépôts sont disponibles 
sur d’autres plateformes. Mes dépôts GitHub (sauf celui-ci, évidemment) ont été
supprimés. Du fait de l’importance de GitHub, je ne peux pas me permettre d’y 
être absent. Mais je peux limiter la casse en liant vers d’autres plateformes, 
en attendant de pouvoir m’héberger moi-même.

> [Pourquoi 301?](https://fr.wikipedia.org/wiki/Erreur_HTTP_301)

> [Pourquoi Microsoft ça pue ?](http://sebsauvage.net/wiki/doku.php?id=microsoft)

## The list / La liste

### Pas maintenus

- [vim-epub](https://framagit.org/etnadji/vim-epub)

    EPUB plugin for Vim.

- [wololo](https://framagit.org/etnadji/wololo)

    Post link(s) to a Wallabag instance

- [evxp](https://framagit.org/etnadji/evxp)

    My XML plugin for Vim.

- [marginaliaEditor](https://framagit.org/etnadji/marginaliaEditor)

    A trial of decent free (libre) XML IDE.

- [internet-cultural-programs](https://framagit.org/etnadji/internet-cultural-programs)

    A giant list of serious / cultural shows on the Internet.
